import * as actionType from '../actions/actionTypes';

const initialState = {
  coffeeList: null,
  error: '',
  loading: false,
  id: ''
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.ORDER_REQUEST:
      return {...state, loading: true};
    case actionType.CLOSE_LOADER:
      return {...state, loading: false};
    case actionType.ORDER_SUCCESS:
      return {...state, coffeeList: action.data};
    case actionType.ORDER_FAILURE:
      return {...state, error: action.error};
    case actionType.GET_ID:
      return {...state, id: action.id};
    default:
      return state;
  }
};

export default reducer;