import * as actionType from '../actions/actionTypes';

const initialState = {
  loading: false,
  error: ''
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.ADD_NEW_DISH_REQUEST:
      return {...state, loading: true};
    case actionType.CLOSE_LOADER:
      return {...state, loading: false};
    case actionType.ADD_NEW_DISH_FAILURE:
      return {...state, error: action.error};
    default:
      return state;
  }
};

export default reducer;