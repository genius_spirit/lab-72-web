import * as actionType from "../actions/actionTypes";

const initialState = {
  ordersList: null,
  loading: false,
  error: ''
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.GET_ORDER_REQUEST:
      return {...state, loading: true};
    case actionType.CLOSE_LOADER:
      return {...state, loading: false};
    case actionType.GET_ORDER_SUCCESS:
      return {...state, ordersList: action.data};
    case actionType.GET_ORDER_FAILURE:
      return {...state, error: action.error};
    default:
      return state;
  }
};

export default reducer;