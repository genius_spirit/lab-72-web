import axios from 'axios';
import * as actionType from "./actionTypes";
import {closeLoader} from "./addNew";

export const orderRequest = () => {
  return {type: actionType.ORDER_REQUEST};
};

export const orderSuccess = (data) => {
  return {type: actionType.ORDER_SUCCESS, data};
};

export const  orderFailure = (error) => {
  return {type: actionType.ORDER_FAILURE, error};
};

export const getCoffeeList = () => {
  return dispatch => {
    dispatch(orderRequest());
    axios.get('/dishes.json').then(response => {
      dispatch(orderSuccess(response.data));
      dispatch(closeLoader());
    }, error => {
      dispatch(orderFailure(error));
    })
  }
};

export const getId = (id) => {
  return {type: actionType.GET_ID, id};
};

export const removeDish = (id, redirect) => dispatch =>{
  axios.delete(`/dishes/${id}.json`);
  redirect('/');
  dispatch(getCoffeeList());
};

export const editDish = (id, data) => dispatch => {
  axios.patch(`/dishes/${id}.json`, data);
};