import {closeLoader} from "./addNew";
import axios from "axios/index";
import * as actionType from "./actionTypes";

export const getOrdersRequest = () => {
  return {type: actionType.GET_ORDER_REQUEST};
};

export const getOrdersSuccess = (data) => {
  return {type: actionType.GET_ORDER_SUCCESS, data};
};

export const getOrdersFailure = (error) => {
  return {type: actionType.GET_ORDER_FAILURE, error};
};

export const getOrdersList = () => dispatch => {
  dispatch(getOrdersRequest());
  axios.get('/dishesOrder.json').then(response => {
    dispatch(getOrdersSuccess(response.data));
    dispatch(closeLoader());
  }, error => {
    dispatch(getOrdersFailure(error));
  })
};

export const removeOrder = (id) => dispatch => {
  dispatch(getOrdersRequest());
  axios.delete(`/dishesOrder/${id}.json`).then(() => {
    dispatch(closeLoader());
    dispatch(getOrdersList());
  }, error => {
    dispatch(getOrdersFailure(error));
  })
};