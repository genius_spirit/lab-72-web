import * as actionType from "./actionTypes";
import axios from "axios/index";

export const closeLoader = () => {
 return {type: actionType.CLOSE_LOADER};
};

export const addNewDishRequest = () => {
  return {type: actionType.ADD_NEW_DISH_REQUEST};
};

export const addNewDishSuccess = () => {
  return {type: actionType.ADD_NEW_DISH_SUCCESS};
};

export const  addNewDishFailure = (error) => {
  return {type: actionType.ADD_NEW_DISH_FAILURE, error};
};

export const onAddNewDish = (data, redirect) => {
  return dispatch => {
    dispatch(addNewDishRequest());
    axios.post('dishes/.json', data).then(() => {
      dispatch(addNewDishSuccess());
      dispatch(closeLoader());
      redirect("/")
    }, error => {
      dispatch(addNewDishFailure(error));
    })
  }
};