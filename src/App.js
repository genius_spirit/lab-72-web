import React, { Component } from 'react';
import {Route, Switch} from "react-router-dom";
import Layout from "./components/Layout/Layout";
import CoffeeShop from "./containers/CoffeeShop/CoffeeShop";
import AddNewDish from "./containers/AddNewDish/AddNewDish";
import Orders from "./containers/Orders/Orders";
import EditDish from "./containers/EditDish/EditDish";

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path="/" exact component={CoffeeShop}/>
          <Route path="/add-new-dish" exact component={AddNewDish}/>
          <Route path="/orders" exact component={Orders}/>
          <Route path="/edit" exact component={EditDish}/>
          <Route render={() => <h1>Page not found</h1>}/>
        </Switch>
      </Layout>
    );
  }
}

export default App;
