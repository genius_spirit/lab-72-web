import React, { Component } from 'react';
import {connect} from "react-redux";
import {Icon} from "material-ui";
import {getOrdersList, removeOrder} from "../../store/actions/orders";
import Spinner from "../../components/UI/Spinner/Spinner";
import './Orders.css';
import {getCoffeeList} from "../../store/actions/coffee";

class Orders extends Component {

  componentDidMount() {
    this.props.getCoffeeList();
    this.props.getOrdersList();
  }

  getTotalPrice = order => {
    return Object.keys(this.props.orders[order]).reduce((acc, coffeeKey) => {
      acc += parseInt(this.props.coffeeList[coffeeKey].price) * this.props.orders[order][coffeeKey];
      return acc;
    }, 150);
  };

  render() {
    return(
      this.props.loading
        ? <Spinner/>
        : <div className="Orders-container">
          <h2>Заказы:</h2>
          {this.props.orders && this.props.coffeeList ?
            Object.keys(this.props.orders).map(order => {
            return (
              <div className="Order" key={order}>
                <div className="Order-list">
                  {Object.keys(this.props.orders[order]).map((orderItem, index) => {
                    return (
                      <div className="Orders-item" key={index}>
                        <span>{this.props.coffeeList[orderItem].title}</span>
                        <span> x {this.props.orders[order][orderItem]}</span>
                        <span>{this.props.coffeeList[orderItem].price} kgs</span>
                      </div>
                    )})
                  }
                </div>
                <div className="Order-total">
                  <span>Доставка: 150 kgs</span>
                  <span>Общая сумма заказа: {this.getTotalPrice(order)}</span>
                  <span onClick={() => this.props.removeOrder(order)}><Icon className="submit">thumb_up</Icon>Завершить заказ</span>
                </div>
              </div>
            )})
          :
          null
          }
        </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    coffeeList: state.coffee.coffeeList,
    orders: state.orders.ordersList,
    loading: state.orders.loading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getOrdersList: () => dispatch(getOrdersList()),
    getCoffeeList: () => dispatch(getCoffeeList()),
    removeOrder: (id) => dispatch(removeOrder(id))
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(Orders);