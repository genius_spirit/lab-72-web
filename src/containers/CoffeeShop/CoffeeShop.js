import React, {Component} from 'react';
import {connect} from "react-redux";
import './CoffeeShop.css';
import {getCoffeeList, getId, removeDish} from "../../store/actions/coffee";
import CoffeeShopItem from "../../components/CoffeeShopItem/CoffeeShopItem";
import Spinner from "../../components/UI/Spinner/Spinner";

class CoffeeShop extends Component {
  componentDidMount() {
    this.props.getCoffeeList();
  }

  render() {
    return(
      <div className="coffeeList">
        { !this.props.loading && this.props.coffeeList
          ? Object.keys(this.props.coffeeList).map(itemId => {
            return (
              <CoffeeShopItem
                src={this.props.coffeeList[itemId].image}
                name={this.props.coffeeList[itemId].title}
                price={this.props.coffeeList[itemId].price}
                key={itemId}
                removed={() => this.props.removeDish(itemId, this.props.history.push)}
                edited={() => this.props.getId(itemId)}
              />
            )
          })
          : <Spinner/> }
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    coffeeList: state.coffee.coffeeList,
    loading: state.coffee.loading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getCoffeeList: () => dispatch(getCoffeeList()),
    removeDish: (id, redirect) => dispatch(removeDish(id, redirect)),
    getId: (id) => dispatch(getId(id))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(CoffeeShop);