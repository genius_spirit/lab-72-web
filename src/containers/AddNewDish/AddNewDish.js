import React, { Component } from 'react';
import {connect} from "react-redux";
import {Button} from "material-ui";
import Spinner from "../../components/UI/Spinner/Spinner";
import './AddNewDish.css';
import {onAddNewDish} from "../../store/actions/addNew";

class AddNewDish extends Component {
  state = {
    title: '',
    price: '',
    image: ''
  };

  valueChanged = (e) => {
    const name = e.target.name;
    this.setState({[name]: e.target.value});
  };

  orderHandler = (event) => {
    event.preventDefault();
    const data = {
      title: this.state.title,
      price: this.state.price,
      image: this.state.image
    };
    this.props.onAddNewDish(data, this.props.history.push);
  };

  render() {
    return (
      <div className="AddContainer">
        <h2>Добавление новой позиции в магазин:</h2>
        {!this.props.loading ?
          <form>
            <input className="addNew-input" type="text" name="title" placeholder="Наименование товара"
                   value={this.state.title} onChange={this.valueChanged}/>
            <input className="addNew-input" type="number" name="price" placeholder="Цена товара"
                   value={this.state.price} onChange={this.valueChanged}/>
            <input className="addNew-input" type="text" name="image" placeholder="Ссылка на картинку"
                   value={this.state.image} onChange={this.valueChanged}/>
            <Button className="addNew-btn" variant="raised" color="primary" onClick={this.orderHandler}>Добавить</Button>
          </form> : <Spinner/>}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.addNew.loading
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onAddNewDish: (data, redirect) => dispatch(onAddNewDish(data, redirect))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddNewDish);