import React, { Component } from 'react';
import {connect} from "react-redux";
import {Button, Input} from "material-ui";
import Spinner from "../../components/UI/Spinner/Spinner";
import './EditDish.css';
import {Link} from "react-router-dom";
import {editDish} from "../../store/actions/coffee";

class EditDish extends Component {
  state = {
    title: this.props.coffeeList[this.props.id].title,
    price: this.props.coffeeList[this.props.id].price,
    image: this.props.coffeeList[this.props.id].image
  };

  valueChanged = (e) => {
    const name = e.target.name;
    this.setState({[name]: e.target.value});
  };

  orderHandler = (event) => {
    event.preventDefault();
    const data = {
      title: this.state.title,
      price: this.state.price,
      image: this.state.image
    };
    this.props.onEditDish(this.props.id, data);
    this.props.history.push('/');
  };

  render() {
    return (
      <div className="AddContainer">
        <h2>Редактирование продукции в магазине:</h2>
        {!this.props.loading ?
          <form>
            <Input className="addNew-input" type="text" name="title" placeholder="Наименование товара"
                   value={this.state.title} onChange={this.valueChanged}/>
            <Input className="addNew-input" type="number" name="price" placeholder="Цена товара"
                   value={this.state.price} onChange={this.valueChanged}/>
            <Input className="addNew-input" type="text" name="image" placeholder="Ссылка на картинку"
                   value={this.state.image} onChange={this.valueChanged}/>
            <div>
              <Button className="back-btn" variant="raised"
                      component={Link} to="/" style={{margin: '10px 15px'}}>Назад</Button>
              <Button className="addNew-btn" variant="raised" color="primary"
                      onClick={this.orderHandler} style={{margin: '10px 15px'}}>Редактировать</Button>
            </div>
          </form> : <Spinner/>}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    id: state.coffee.id,
    coffeeList: state.coffee.coffeeList
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onEditDish: (id, data) => dispatch(editDish(id, data))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditDish);