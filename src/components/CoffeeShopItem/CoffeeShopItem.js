import React from 'react';
import './CoffeeShopItem.css';
import {Link} from "react-router-dom";

const CoffeeShopItem = (props) => {
  return(
    <div className="coffeeList__item">
      <div className="coffeeList__img"><img src={props.src} alt="coffee"/></div>
      <div className="coffeeList__title">
        <p className="coffeeList__name">{props.name}</p>
        <p className="coffeeList__price">цена: {props.price} kgs</p>
      </div>
      <div className="coffeeList__btn">
        <span><i className="material-icons md" onClick={props.removed}>delete_sweep</i>remove</span>
        <Link to="/edit" onClick={props.edited}><span><i className="material-icons md">mode_edit</i>edit</span></Link>
      </div>
    </div>
  )
};

export default CoffeeShopItem;