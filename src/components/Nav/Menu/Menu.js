import React from 'react';
import './Menu.css';
import {NavLink} from "react-router-dom";

const Menu = () => {
  return(
    <div className="Menu">
      <ul className="Menu-list">
        <li><NavLink to="/" >Магазин</NavLink></li>
        <li><NavLink to="/orders" >Заказы</NavLink></li>
        <li><NavLink to="/add-new-dish">
          <i className="addNew material-icons">add_circle</i>
        </NavLink></li>
      </ul>
    </div>
  )
};

export default Menu;