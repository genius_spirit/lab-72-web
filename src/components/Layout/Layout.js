import React from 'react';
import './Layout.css';
import Toolbar from "../Nav/Toolbar/Toolbar";

const Layout = (props) => {
  return(
    <div className="container">
      <Toolbar/>
      <main className="Layout-Content">
        {props.children}
      </main>
    </div>
  )
};

export default Layout;