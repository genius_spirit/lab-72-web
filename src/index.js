import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import {Provider} from "react-redux";
import thunk from "redux-thunk";
import {BrowserRouter} from "react-router-dom";
import axios from "axios";
import './index.css';
import App from './App';
import coffeeReducer from "./store/reducers/coffee";
import addNewReducer from "./store/reducers/addNew";
import ordersReducer from "./store/reducers/orders";

axios.defaults.baseURL = 'https://blog-187e7.firebaseio.com';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
  coffee: coffeeReducer,
  addNew: addNewReducer,
  orders: ordersReducer
});
const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));

ReactDOM.render(<BrowserRouter><Provider store={store}><App /></Provider></BrowserRouter>, document.getElementById('root'));
registerServiceWorker();
